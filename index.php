<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<!--     
    1. create a form in index.php that would get the following input from the user
			Day of Birth
			Month of Birth 

	2. when the user inputs the data ...the information will be submitted to the index_action.php and it will processs the users data and determine the users
	Zodiac Sign.  -->
    <div class="container mt-5 text-left justify-content-start">
    <form method="POST" action="/index_actions.php">
        <div class="container">
            <label for="month">What is your birth month</label>
            <input type="number" id="month" name="month" class="form-control w-25 ml-3 d-inline">
        </div>
        <div class="container">
            <label for="day">What is your birth day</label>
            <input type="number" id="day" name="day" class="form-control w-25 ml-3 d-inline">
        </div>
        <button type="submit" class="btn btn-submit">Submit</button>
    </form>

    
</body>
</html>